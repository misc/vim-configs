" Vim syntax file
" Language: EET
"
" This syntax file supports the textual representation of eet files. That is,
" the output of `eet -d` (conversely, the input of `eet -i` and `eet -e`).
" This is convenient when `vieet` is used.

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn keyword eetSection group value key count
syn keyword eetType struct array var_array list hash uint string uchar char
syn keyword eetType short int long_long float double ushort ulong_long
syn keyword eetType inlined
syn keyword eetNull null
syn match eetNumber display "\d\+"
syn region eetString start=+L\="+ end=+"+

hi def link eetSection Label
hi def link eetType Type
hi def link eetNumber Number
hi def link eetString String
hi def link eetNull Constant

let b:current_syntax = "eet"
